# HackerRank Solutions
-----------------------
My solutions to [HackerRank](https://www.hackerrank.com/) problems. I've tried to organize the solutions in a manner that mimics how they're structured on HackerRank.

* [Algorithms](/algorithms)
* [Data Structures](/data_structures)
* [Python](/python)
* [SQL](/sql)

Thus far there are solutions to **155** distinct problems across the above areas. Lastly, if you're curious, you can view my HackerRank [profile](https://www.hackerrank.com/latino_chingon).