if __name__ == '__main__':
    n = int(input())
    arr = list(map(int, input().split()))

    # Obtain top score
    top_score = max(arr)

    # Eliminate all instances of the top score
    all_other_scores = [val for val in arr if val != top_score]

    # Our max will now be for the 2nd best score
    print(max(all_other_scores))
