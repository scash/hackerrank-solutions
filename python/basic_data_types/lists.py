def exec_commands(commands: list):
    result = list()

    for i in range(len(commands)):
        if commands[i][0] == "insert":
            result.insert(int(commands[i][1]), int(commands[i][2]))
        elif commands[i][0] == "print":
            print(result)
        elif commands[i][0] == "remove":
            result.remove(int(commands[i][1]))
        elif commands[i][0] == "append":
            result.append(int(commands[i][1]))
        elif commands[i][0] == "sort":
            result.sort()
        elif commands[i][0] == "pop":
            result.pop()
        else:
            result.reverse()



if __name__ == '__main__':
    commands = list()

    N = int(input())

    # Retrieve list of command inputs
    for _ in range(N):
        commands.append(input().rstrip().split())

    exec_commands(commands)
