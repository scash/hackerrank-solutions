if __name__ == '__main__':
    students = list()

    for _ in range(int(input())):
        name = input()
        score = float(input())

        students.append([name, score])

    # Obtain lowest score
    lowest_score = min(x[1] for x in students)

    # Eliminate student(s) with lowest score from list of students
    students = [student for student in students if student[1] != lowest_score]

    # Obtain 2nd lowest grade
    second_lowest_score = min(x[1] for x in students)

    # Retrieve student(s) with 2nd lowest grade
    students = [student[0] for student in students if student[1] == second_lowest_score]

    # Sort list
    students.sort()

    for i in range(len(students)):
        print(students[i])
