# Python
-----------------------
### Introduction
* [Easy] [Hello, World!](https://www.hackerrank.com/challenges/py-hello-world) (5 pts) {solution in [python](/python/introduction/hello_world.py)}
* [Easy] [If-Else](https://www.hackerrank.com/challenges/py-if-else) (10 pts) {solution in [python](/python/introduction/if_else.py)}
* [Easy] [Arithmetic Operators](https://www.hackerrank.com/challenges/python-arithmetic-operators) (10 pts) {solution in [python](/python/introduction/arithmetic_operators.py)}
* [Easy] [Division](https://www.hackerrank.com/challenges/python-division) (10 pts) {solution in [python](/python/introduction/division.py)}
* [Easy] [Loops](https://www.hackerrank.com/challenges/python-loops) (10 pts) {solution in [python](/python/introduction/loops.py)}
* [Medium] [Write a Function](https://www.hackerrank.com/challenges/write-a-function) (10 pts) {solution in [python](/python/introduction/write_a_function.py)}
* [Easy] [Print Function](https://www.hackerrank.com/challenges/python-print) (20 pts) {solution in [python](/python/introduction/print_function.py)}

### Basic Data Types
* [Easy] [List Comprehensions](https://www.hackerrank.com/challenges/list-comprehensions) (10 pts) {solution in [python](/python/basic_data_types/list_comprehensions.py)}
* [Easy] [Find the Runner-Up Score!](https://www.hackerrank.com/challenges/find-second-maximum-number-in-a-list) (10 pts) {solution in [python](/python/basic_data_types/find_the_runner_up_score.py)}
* [Easy] [Nested Lists](https://www.hackerrank.com/challenges/nested-list) (10 pts) {solution in [python](/python/basic_data_types/nested_lists.py)}
* [Easy] [Finding the Percentage](https://www.hackerrank.com/challenges/finding-the-percentage) (10 pts) {solution in [python](/python/basic_data_types/finding_the_percentage.py)}
* [Easy] [Lists](https://www.hackerrank.com/challenges/python-lists) (10 pts) {solution in [python](/python/basic_data_types/lists.py)}
* [Easy] [Tuples](https://www.hackerrank.com/challenges/python-tuples) (10 pts) {solution in [python](/python/basic_data_types/tuples.py)}

### Strings
* [Easy] [sWAP cASE](https://www.hackerrank.com/challenges/swap-case) (10 pts) {solution in [python](/python/strings/swap_case.py)}
* [Easy] [String Split and Join](https://www.hackerrank.com/challenges/python-string-split-and-join) (10 pts) {solution in [python](/python/strings/string_split_and_join.py)}
* [Easy] [What's Your Name?](https://www.hackerrank.com/challenges/whats-your-name) (10 pts) {solution in [python](/python/strings/what_is_your_name.py)}
* [Easy] [Mutations](https://www.hackerrank.com/challenges/python-mutations) (10 pts) {solution in [python](/python/strings/mutations.py)}
* [Easy] [Find a String](https://www.hackerrank.com/challenges/find-a-string) (10 pts) {solution in [python](/python/strings/find_a_string.py)}
* [Easy] [String Validators](https://www.hackerrank.com/challenges/string-validators) (10 pts) {solution in [python](/python/strings/string_validators.py)}
* [Easy] [Text Alignment](https://www.hackerrank.com/challenges/text-alignment) (10 pts) {solution in [python](/python/strings/text_alignment.py)}
* [Easy] [Text Wrap](https://www.hackerrank.com/challenges/text-wrap) (10 pts) {solution in [python](/python/strings/text_wrap.py)}
* [Easy] [Designer Door Mat](https://www.hackerrank.com/challenges/designer-door-mat) (10 pts) {solution in [python](/python/strings/designer_door_mat.py)}
* [Easy] [String Formatting](https://www.hackerrank.com/challenges/python-string-formatting) (10 pts) {solution in [python](/python/strings/string_formatting.py)}
* [Easy] [Alphabet Rangoli](https://www.hackerrank.com/challenges/alphabet-rangoli) (20 pts) {solution in [python](/python/strings/alphabet_rangoli.py)}
* [Easy] [Capitalize!](https://www.hackerrank.com/challenges/capitalize) (20 pts) {solution in [python](/python/strings/capitalize.py)}

### Sets
* [Easy] [Introduction to Sets](https://www.hackerrank.com/challenges/py-introduction-to-sets) (10 pts) {solution in [python](/python/sets/introduction_to_sets.py)}
* [Medium] [No Idea!](https://www.hackerrank.com/challenges/no-idea) (50 pts) {solution in [python](/python/sets/no_idea.py)}
* [Easy] [Symmetric Difference](https://www.hackerrank.com/challenges/symmetric-difference) (10 pts) {solution in [python](/python/sets/symmetric_difference.py)}
* [Easy] [Set .add()](https://www.hackerrank.com/challenges/py-set-add) (10 pts) {solution in [python](/python/sets/set_add.py)}
* [Easy] [Set .discard(), .remove() & .pop()](https://www.hackerrank.com/challenges/py-set-discard-remove-pop) (10 pts) {solution in [python](/python/sets/set_discard_remove_pop.py)}
* [Easy] [Set .union() Operation](https://www.hackerrank.com/challenges/py-set-union) (10 pts) {solution in [python](/python/sets/set_union_operation.py)}
* [Easy] [Set .intersection() Operation](https://www.hackerrank.com/challenges/py-set-intersection-operation) (10 pts) {solution in [python](/python/sets/set_intersection_operation.py)}
* [Easy] [Set .difference() Operation](https://www.hackerrank.com/challenges/py-set-difference-operation) (10 pts) {solution in [python](/python/sets/set_difference_operation.py)}