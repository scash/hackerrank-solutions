def happiness(arr: list, a: set, b: set) -> int:
    unique_vals = set(arr)

    # Obtain set of integers found in both sets
    in_both = a & b

    # Eliminate integers found in both sets
    new_a = a - in_both
    new_b = b - in_both

    # Eliminate integers from set that are not present
    #   in the array as they will not impact happiness
    new_a &= unique_vals
    new_b &= unique_vals

    a_happiness = sum([1 for val in arr if val in new_a])
    b_happiness = sum([1 for val in arr if val in new_b])

    return a_happiness - b_happiness



if __name__ == '__main__':
    n, m = input().split()

    arr = list(map(int, input().split()))
    a = set(map(int, input().split()))
    b = set(map(int, input().split()))

    result = happiness(arr, a, b)

    print(result)
