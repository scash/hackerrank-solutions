def diff(m: set, n: set) -> list:
    all_vals = m.union(n)
    in_both = m.intersection(n)

    output = sorted(all_vals.difference(in_both))

    return output



if __name__ == '__main__':
    m = input()
    m_set = set(map(int, input().split()))
    n = input()
    n_set = set(map(int, input().split()))

    output = diff(m_set, n_set)

    for item in output:
        print(item)
