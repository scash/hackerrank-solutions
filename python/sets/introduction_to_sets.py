def average(arr):
    unique_heights = set(arr)

    return sum(unique_heights) / len(unique_heights)



if __name__ == '__main__':
    n = int(input())
    arr = list(map(int, input().split()))

    result = average(arr)

    print(result)
