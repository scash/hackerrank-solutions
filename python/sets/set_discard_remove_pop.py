n = int(input())
s = set(map(int, input().split()))
cmds = int(input())

for i in range(cmds):
    cmd = input()

    if cmd == "pop":
        s.pop()
        continue

    cmd, val = cmd.split()

    if cmd == "discard":
        s.discard(int(val))
    else:
        s.remove(int(val))

print(sum(s))
