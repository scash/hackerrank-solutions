def mutate_string(string, position, character):
    output = list(string)

    output[position] = character

    return "".join(output)



if __name__ == '__main__':
    s = input()
    i, c = input().split()

    s_new = mutate_string(s, int(i), c)

    print(s_new)
