if __name__ == '__main__':
    alnum, al, dig, lc, uc = False, False, False, False, False

    s = input()

    for c in s:
        if c.isalnum():
            alnum = True

        if c.isalpha():
            al = True

        if c.isdigit():
            dig = True
        
        if c.islower():
            lc = True
        
        if c.isupper():
            uc = True

        # Stop search if all statements are True
        if alnum and al and dig and lc and uc:
            break

    print(alnum, al, dig, lc, uc, sep="\n")
