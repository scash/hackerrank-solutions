def print_formatted(number):
    pad = len("{:b}".format(number))

    for i in range(1, number + 1):
        output = "{0:{pad}d} {0:{pad}o} {0:{pad}X} {0:{pad}b}".format(i, pad=pad)
        print(output)



if __name__ == '__main__':
    n = int(input())
    print_formatted(n)
