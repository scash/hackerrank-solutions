def count_substring(string, sub_string):
    index, output = 0, 0

    while index >= 0:
        index = string.find(sub_string, index)

        if index >= 0:
            output += 1

            # Begin next search after the current match
            index += 1

    return output



if __name__ == '__main__':
    string = input().strip()
    sub_string = input().strip()

    count = count_substring(string, sub_string)
    print(count)
