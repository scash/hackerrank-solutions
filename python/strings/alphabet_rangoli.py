def print_rangoli(size):
    chars = list()
    width = size + ((size - 1) * 3)

    ########################################################
    # Handle top and middle parts of the alphabet rangoli
    ########################################################

    # Grab desired characters in reverse order
    for i in range((ord("a") + size) - 1, ord("a") - 1, -1):
        chars.append(chr(i))

        # Form left and middle part of the row by adding
        #   a dash between all existing characters
        row_text = "-".join(chars)

        # Eliminate last char, reverse, and then
        #   append to the right of the row string
        row_text += row_text[:-1][::-1]

        # Center row string.
        #   Add dashes to pad to desired length
        print(row_text.center(width, "-"))

    ########################################################
    # Handle bottom part of the alphabet rangoli
    ########################################################

    # Eliminate "a" as it's no longer needed
    chars.remove("a")

    # Traverse, in reverse order, remaining characters
    for i in range(len(chars) - 1, -1, -1):

        row_text = "-".join(chars)
        row_text += row_text[:-1][::-1]

        del chars[i]

        print(row_text.center(width, "-"))



if __name__ == '__main__':
    n = int(input())

    print_rangoli(n)
