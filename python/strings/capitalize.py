#!/bin/python3

import os

def solve(s):
    output = ""
    is_space = True

    for c in s:
        if c == " ":
            is_space = True
        elif is_space:
            is_space = False
            c = c.capitalize()

        output += c

    return output



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = solve(s)

    fptr.write(result + '\n')

    fptr.close()
