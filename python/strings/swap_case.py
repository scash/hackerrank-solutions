def swap_case(s):
    swap_s = ""

    for char in s:
        if char.islower():
            swap_s += char.upper()
        else:
            swap_s += char.lower()

    return swap_s

    #
    # Alternate solution
    #
    # swapped = [c.upper() if c.islower() else c.lower() for c in s]
    #
    # return "".join(swapped)



if __name__ == '__main__':
    s = input()
    result = swap_case(s)
    print(result)
