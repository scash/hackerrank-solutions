cin = list(map(int, input().rstrip().split()))

# N - 1, per requirements, will always result in an even number
triangle_depth = cin[0] - 1

# Start at 1 and skip even numbers
for i in range(1, triangle_depth, 2):
    mid = ".|." * i
    print(mid.center(cin[1], "-"))

print("WELCOME".center(cin[1], "-"))

# Step by 2, going backwards, to skip even numbers
for i in range(triangle_depth - 1, 0, -2):
    mid = ".|." * i
    print(mid.center(cin[1], "-"))
