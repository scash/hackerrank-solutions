# SQL
-----------------------
### Basic Select
* [Easy] [Revising the Select Query I](https://www.hackerrank.com/challenges/revising-the-select-query) (10 pts) {solution in [sql](/sql/basic_select/revising_the_select_query_1.sql)}
* [Easy] [Revising the Select Query II](https://www.hackerrank.com/challenges/revising-the-select-query-2) (10 pts) {solution in [sql](/sql/basic_select/revising_the_select_query_2.sql)}
* [Easy] [Select All](https://www.hackerrank.com/challenges/select-all-sql) (10 pts) {solution in [sql](/sql/basic_select/select_all.sql)}
* [Easy] [Select By ID](https://www.hackerrank.com/challenges/select-by-id) (10 pts) {solution in [sql](/sql/basic_select/select_by_id.sql)}
* [Easy] [Japanese Cities' Attributes](https://www.hackerrank.com/challenges/japanese-cities-attributes) (10 pts) {solution in [sql](/sql/basic_select/japanese_cities_attributes.sql)}
* [Easy] [Japanese Cities' Names](https://www.hackerrank.com/challenges/japanese-cities-name) (10 pts) {solution in [sql](/sql/basic_select/japanese_cities_names.sql)}
* [Easy] [Weather Observation Station 1](https://www.hackerrank.com/challenges/weather-observation-station-1) (15 pts) {solution in [sql](/sql/basic_select/weather_observation_station_1.sql)}
* [Easy] [Weather Observation Station 3](https://www.hackerrank.com/challenges/weather-observation-station-3) (10 pts) {solution in [mysql](/sql/basic_select/weather_observation_station_3.sql)}
* [Easy] [Weather Observation Station 4](https://www.hackerrank.com/challenges/weather-observation-station-4) (10 pts) {solution in [sql](/sql/basic_select/weather_observation_station_4.sql)}
* [Easy] [Weather Observation Station 5](https://www.hackerrank.com/challenges/weather-observation-station-5) (30 pts) {solution in [mysql](/sql/basic_select/weather_observation_station_5.sql)}
* [Easy] [Weather Observation Station 6](https://www.hackerrank.com/challenges/weather-observation-station-6) (10 pts) {solution in [mysql](/sql/basic_select/weather_observation_station_6.sql)}
* [Easy] [Weather Observation Station 7](https://www.hackerrank.com/challenges/weather-observation-station-7) (10 pts) {solution in [mysql](/sql/basic_select/weather_observation_station_7.sql)}
* [Easy] [Weather Observation Station 8](https://www.hackerrank.com/challenges/weather-observation-station-8) (15 pts) {solution in [mysql](/sql/basic_select/weather_observation_station_8.sql)}
* [Easy] [Weather Observation Station 9](https://www.hackerrank.com/challenges/weather-observation-station-9) (10 pts) {solution in [mysql](/sql/basic_select/weather_observation_station_9.sql)}
* [Easy] [Weather Observation Station 10](https://www.hackerrank.com/challenges/weather-observation-station-10) (10 pts) {solution in [mysql](/sql/basic_select/weather_observation_station_10.sql)}
* [Easy] [Weather Observation Station 11](https://www.hackerrank.com/challenges/weather-observation-station-11) (15 pts) {solution in [mysql](/sql/basic_select/weather_observation_station_11.sql)}
* [Easy] [Weather Observation Station 12](https://www.hackerrank.com/challenges/weather-observation-station-12) (15 pts) {solution in [mysql](/sql/basic_select/weather_observation_station_12.sql)}
* [Easy] [Higher Than 75 Marks](https://www.hackerrank.com/challenges/more-than-75-marks) (15 pts) {solution in [mysql](/sql/basic_select/higher_than_75_marks.sql)}
* [Easy] [Employee Names](https://www.hackerrank.com/challenges/name-of-employees) (10 pts) {solution in [sql](/sql/basic_select/employee_names.sql)}
* [Easy] [Employee Salaries](https://www.hackerrank.com/challenges/salary-of-employees) (10 pts) {solution in [sql](/sql/basic_select/employee_salaries.sql)}

### Advanced Select
* [Easy] [Type of Triangle](https://www.hackerrank.com/challenges/what-type-of-triangle) (20 pts) {solution in [mysql](/sql/advanced_select/type_of_triangle.sql)}
* [Medium] [The PADS](https://www.hackerrank.com/challenges/the-pads) (30 pts) {solution in [mysql](/sql/advanced_select/the_pads.sql)}
* [Medium] [Binary Tree Nodes](https://www.hackerrank.com/challenges/binary-search-tree-1) (30 pts) {solution in [mysql](/sql/advanced_select/binary_tree_nodes.sql)}
* [Medium] [New Companies](https://www.hackerrank.com/challenges/the-company) (30 pts) {solution in [mysql](/sql/advanced_select/new_companies.sql)}

### Aggregation
* [Easy] [Revising Aggregations - The Count Function](https://www.hackerrank.com/challenges/revising-aggregations-the-count-function) (10 pts) {solution in [sql](/sql/aggregation/the_count_function.sql)}
* [Easy] [Revising Aggregations - The Sum Function](https://www.hackerrank.com/challenges/revising-aggregations-sum) (10 pts) {solution in [sql](/sql/aggregation/the_sum_function.sql)}
* [Easy] [Revising Aggregations - Averages](https://www.hackerrank.com/challenges/revising-aggregations-the-average-function) (10 pts) {solution in [sql](/sql/aggregation/averages.sql)}
* [Easy] [Average Population](https://www.hackerrank.com/challenges/average-population) (10 pts) {solution in [mysql](/sql/aggregation/average_population.sql)}
* [Easy] [Japan Population](https://www.hackerrank.com/challenges/japan-population) (10 pts) {solution in [sql](/sql/aggregation/japan_population.sql)}
* [Easy] [Population Density Difference](https://www.hackerrank.com/challenges/population-density-difference) (10 pts) {solution in [sql](/sql/aggregation/population_density_difference.sql)}
* [Easy] [The Blunder](https://www.hackerrank.com/challenges/the-blunder) (15 pts) {solution in [mysql](/sql/aggregation/the_blunder.sql)}
* [Easy] [Top Earners](https://www.hackerrank.com/challenges/earnings-of-employees) (20 pts) {solution in [sql](/sql/aggregation/top_earners.sql)}
* [Easy] [Weather Observation Station 2](https://www.hackerrank.com/challenges/weather-observation-station-2) (15 pts) {solution in [mysql](/sql/aggregation/weather_observation_station_2.sql)}
* [Easy] [Weather Observation Station 13](https://www.hackerrank.com/challenges/weather-observation-station-13) (10 pts) {solution in [mysql](/sql/aggregation/weather_observation_station_13.sql)}
* [Easy] [Weather Observation Station 14](https://www.hackerrank.com/challenges/weather-observation-station-14) (10 pts) {solution in [mysql](/sql/aggregation/weather_observation_station_14.sql)}
* [Easy] [Weather Observation Station 15](https://www.hackerrank.com/challenges/weather-observation-station-15) (15 pts) {solution in [mysql](/sql/aggregation/weather_observation_station_15.sql)}
* [Easy] [Weather Observation Station 16](https://www.hackerrank.com/challenges/weather-observation-station-16) (10 pts) {solution in [mysql](/sql/aggregation/weather_observation_station_16.sql)}
* [Easy] [Weather Observation Station 17](https://www.hackerrank.com/challenges/weather-observation-station-17) (15 pts) {solution in [mysql](/sql/aggregation/weather_observation_station_17.sql)}
* [Medium] [Weather Observation Station 18](https://www.hackerrank.com/challenges/weather-observation-station-18) (25 pts) {solution in [mysql](/sql/aggregation/weather_observation_station_18.sql)}
* [Medium] [Weather Observation Station 19](https://www.hackerrank.com/challenges/weather-observation-station-19) (30 pts) {solution in [mysql](/sql/aggregation/weather_observation_station_19.sql)}
* [Medium] [Weather Observation Station 20](https://www.hackerrank.com/challenges/weather-observation-station-20) (40 pts) {solution in [mysql](/sql/aggregation/weather_observation_station_20.sql)}

### Basic Join
* [Easy] [Asian Population](https://www.hackerrank.com/challenges/asian-population) (10 pts) {solution in [sql](/sql/basic_join/asian_population.sql)}
* [Easy] [African Cities](https://www.hackerrank.com/challenges/african-cities) (10 pts) {solution in [sql](/sql/basic_join/african_cities.sql)}
* [Easy] [Average Population of Each Continent](https://www.hackerrank.com/challenges/average-population-of-each-continent) (10 pts) {solution in [mysql](/sql/basic_join/average_population_of_each_continent.sql)}
* [Medium] [The Report](https://www.hackerrank.com/challenges/the-report) (20 pts) {solution in [mysql](/sql/basic_join/the_report.sql)}
* [Medium] [Top Competitors](https://www.hackerrank.com/challenges/full-score) (30 pts) {solution in [sql](/sql/basic_join/top_competitors.sql)}