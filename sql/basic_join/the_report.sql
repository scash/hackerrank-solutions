SELECT IF(Grades.Grade >= 8, Students.Name, NULL), Grades.Grade, Students.Marks
FROM Students
    LEFT JOIN Grades
    ON Students.Marks BETWEEN Grades.Min_Mark AND Grades.Max_Mark
ORDER BY Grades.Grade DESC, 1, Students.Marks