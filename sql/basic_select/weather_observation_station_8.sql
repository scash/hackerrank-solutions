SELECT DISTINCT CITY
FROM STATION
WHERE LCASE(SUBSTR(CITY, 1, 1)) IN ("a", "e", "i", "o", "u")
    AND LCASE(RIGHT(CITY, 1)) IN ("a", "e", "i", "o", "u")