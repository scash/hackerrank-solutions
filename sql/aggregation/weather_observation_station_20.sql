/*
Verbose, but works when the number of rows is even or odd
*/

SET @row_id = 0;

SET @floor_index = (
    SELECT
        CASE
            WHEN COUNT(*) % 2 = 0 THEN FLOOR(COUNT(*) / 2)
            ELSE FLOOR((COUNT(*) + 1) / 2)
        END
    FROM STATION);

SET @ceiling_index = (
    SELECT
        CASE
            WHEN COUNT(*) % 2 = 0 THEN CEIL((COUNT(*) / 2) + 1)
            ELSE 0
        END
    FROM STATION);

/*
Note that when the number of rows is odd, @ceiling_index will be
    zero and hence why the query works with 1 (even # of rows) or
    2 (odd # of rows) entries
*/
SELECT ROUND(AVG(S_ORDERED.LAT_N), 4)
FROM (
    SELECT (@row_id := @row_id + 1) AS RID, LAT_N
    FROM STATION
    ORDER BY LAT_N) AS S_ORDERED
WHERE S_ORDERED.RID IN (@floor_index, @ceiling_index)