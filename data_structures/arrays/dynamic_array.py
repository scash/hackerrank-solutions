#!/bin/python3

import os

#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts following parameters:
#  1. INTEGER n
#  2. 2D_INTEGER_ARRAY queries
#
def dynamicArray(n, queries):
    output = list()
    last_answer = 0

    # Create N empty sequences
    seqs = [list() for _ in range(n)]

    for q, x, y in queries:
        index = (x ^ last_answer) % n

        if q == 1:
            seqs[index].append(y)
        else:
            last_answer = seqs[index][y % len(seqs[index])]
            output.append(last_answer)

    return output



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    q = int(first_multiple_input[1])

    queries = []

    for _ in range(q):
        queries.append(list(map(int, input().rstrip().split())))

    result = dynamicArray(n, queries)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
