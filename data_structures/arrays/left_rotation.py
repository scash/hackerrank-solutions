#!/bin/python3

def left_rotation(d, arr):

    # When requested rotations is >= the size of the array,
    #   we find the remainder to limit the number of performed
    #   rotations. For example, if d = 17 and array is of size 5,
    #   then d should be assigned 2 (hence rotating twice) instead
    #   of 17 times...
    if d >= len(arr):
        d %= len(arr)

    # Performing the requested rotations results in the same array
    if d == 0:
        return arr

    # Split and shift array items (to "mimic" rotation)
    return arr[d:] + arr[:d]



if __name__ == '__main__':
    nd = input().split()

    n = int(nd[0])

    d = int(nd[1])

    arr = list(map(int, input().rstrip().split()))

    result = left_rotation(d, arr)

    output = list(map(str, result))

    print(" ".join(output))
