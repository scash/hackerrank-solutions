#!/bin/python3

import os

class SinglyLinkedListNode:
    def __init__(self, node_data):
        self.data = node_data
        self.next = None

class SinglyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def insert_node(self, node_data):
        node = SinglyLinkedListNode(node_data)

        if not self.head:
            self.head = node
        else:
            self.tail.next = node

        self.tail = node

def print_singly_linked_list(node, sep, fptr):
    while node:
        fptr.write(str(node.data))

        node = node.next

        if node:
            fptr.write(sep)

#
# For your reference:
#
# SinglyLinkedListNode:
#     int data
#     SinglyLinkedListNode next
#
def mergeLists(head1, head2):

    if head1 and head2:
        nodes = list()
        output_head = None
        node1, node2 = head1, head2

        while node1:
            nodes.append(node1)

            node1 = node1.next

        while node2:
            nodes.append(node2)

            node2 = node2.next

        # Sort collection of nodes by data value
        nodes = sorted(nodes, key=lambda n: n.data)

        # Ensure node sequence is properly reflected after merge.
        #   NOTE: Given that the lists being merged are in ascending
        #   order, tail.next should already equal to NONE
        for i in range(1, len(nodes)):
            if i == 1:
                output_head = nodes[i-1]

            nodes[i-1].next = nodes[i]

        return output_head



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    tests = int(input())

    for tests_itr in range(tests):
        llist1_count = int(input())

        llist1 = SinglyLinkedList()

        for _ in range(llist1_count):
            llist1_item = int(input())
            llist1.insert_node(llist1_item)
            
        llist2_count = int(input())

        llist2 = SinglyLinkedList()

        for _ in range(llist2_count):
            llist2_item = int(input())
            llist2.insert_node(llist2_item)

        llist3 = mergeLists(llist1.head, llist2.head)

        print_singly_linked_list(llist3, ' ', fptr)
        fptr.write('\n')

    fptr.close()
