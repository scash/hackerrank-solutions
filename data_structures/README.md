# Data Structures
-----------------------
### Arrays
* [Easy] [Arrays - DS](https://www.hackerrank.com/challenges/arrays-ds) (10 pts) {solution in [python](/data_structures/arrays/arrays_ds.py)}
* [Easy] [2d Array - DS](https://www.hackerrank.com/challenges/2d-array) (15 pts) {solution in [python](/data_structures/arrays/2d_array.py)}
* [Easy] [Dynamic Array](https://www.hackerrank.com/challenges/dynamic-array) (15 pts) {solution in [python](/data_structures/arrays/dynamic_array.py)}
* [Easy] [Left Rotation](https://www.hackerrank.com/challenges/array-left-rotation) (20 pts) {solution in [python](/data_structures/arrays/left_rotation.py)}
* [Medium] [Sparse Arrays](https://www.hackerrank.com/challenges/sparse-arrays) (25 pts) {solution in [python](/data_structures/arrays/sparse_arrays.py)}

### Linked Lists
* [Easy] [Print the Elements of a Linked List](https://www.hackerrank.com/challenges/print-the-elements-of-a-linked-list) (5 pts) {solution in [python](/data_structures/linked_lists/print_the_elements_of_a_linked_list.py)}
* [Easy] [Insert a Node at the Tail of a Linked List](https://www.hackerrank.com/challenges/insert-a-node-at-the-tail-of-a-linked-list) (5 pts) {solution in [python](/data_structures/linked_lists/insert_a_node_at_the_tail_of_a_linked_list.py)}
* [Easy] [Insert a Node at the Head of a Linked List](https://www.hackerrank.com/challenges/insert-a-node-at-the-head-of-a-linked-list) (5 pts) {solution in [python](/data_structures/linked_lists/insert_a_node_at_the_head_of_a_linked_list.py)}
* [Easy] [Insert a Node at a Specific Position in a Linked List](https://www.hackerrank.com/challenges/insert-a-node-at-a-specific-position-in-a-linked-list) (5 pts) {solution in [python](/data_structures/linked_lists/insert_a_node_at_a_specific_position_in_a_linked_list.py)}
* [Easy] [Delete a Node](https://www.hackerrank.com/challenges/delete-a-node-from-a-linked-list) (5 pts) {solution in [python](/data_structures/linked_lists/delete_a_node.py)}
* [Easy] [Print in Reverse](https://www.hackerrank.com/challenges/print-the-elements-of-a-linked-list-in-reverse) (5 pts) {solution in [python](/data_structures/linked_lists/print_in_reverse.py)}
* [Easy] [Reverse a Linked List](https://www.hackerrank.com/challenges/reverse-a-linked-list) (5 pts) {solution in [python](/data_structures/linked_lists/reverse_a_linked_list.py)}
* [Easy] [Compare Two Linked Lists](https://www.hackerrank.com/challenges/compare-two-linked-lists) (5 pts) {solution in [python](/data_structures/linked_lists/compare_two_linked_lists.py)}
* [Easy] [Merge Two Sorted Linked Lists](https://www.hackerrank.com/challenges/merge-two-sorted-linked-lists) (5 pts) {solution in [python](/data_structures/linked_lists/merge_two_sorted_linked_lists.py)}
* [Easy] [Get Node Value](https://www.hackerrank.com/challenges/get-the-value-of-the-node-at-a-specific-position-from-the-tail) (5 pts) {solution in [python](/data_structures/linked_lists/get_node_value.py)}
* [Easy] [Delete Duplicate-Value Nodes From a Sorted Linked List](https://www.hackerrank.com/challenges/delete-duplicate-value-nodes-from-a-sorted-linked-list) (5 pts) {solution in [python](/data_structures/linked_lists/delete_duplicate_value_nodes_from_a_sorted_linked_list.py)}
* [Medium] [Cycle Detection](https://www.hackerrank.com/challenges/detect-whether-a-linked-list-contains-a-cycle) (5 pts) {solution in [python](/data_structures/linked_lists/cycle_detection.py)}