# Algorithms
-----------------------
### Warmup
* [Easy] [Solve Me First](https://www.hackerrank.com/challenges/solve-me-first) (1 pt) {solution in [python](/algorithms/warmup/solve_me_first.py)}
* [Easy] [Simple Array Sum](https://www.hackerrank.com/challenges/simple-array-sum) (10 pts) {solution in [python](/algorithms/warmup/simple_sum_array.py)}
* [Easy] [Compare The Triplets](https://www.hackerrank.com/challenges/compare-the-triplets) (10 pts) {solution in [python](/algorithms/warmup/compare_the_triplets.py)}
* [Easy] [A Very Big Sum](https://www.hackerrank.com/challenges/a-very-big-sum) (10 pts) {solution in [python](/algorithms/warmup/a_very_big_sum.py)}
* [Easy] [Diagonal Difference](https://www.hackerrank.com/challenges/diagonal-difference) (10 pts) {solution in [python](/algorithms/warmup/diagonal_difference.py)}
* [Easy] [Plus Minus](https://www.hackerrank.com/challenges/plus-minus) (10 pts) {solution in [python](/algorithms/warmup/plus_minus.py)}
* [Easy] [Staircase](https://www.hackerrank.com/challenges/staircase) (10 pts) {solution in [python](/algorithms/warmup/staircase.py)}
* [Easy] [Mini-Max Sum](https://www.hackerrank.com/challenges/mini-max-sum) (10 pts) {solution in [python](/algorithms/warmup/mini_max_sum.py)}
* [Easy] [Birthday Cake Candles](https://www.hackerrank.com/challenges/birthday-cake-candles) (10 pts) {solution in [python](/algorithms/warmup/birthday_cake_candles.py)}
* [Easy] [Time Conversion](https://www.hackerrank.com/challenges/time-conversion) (15 pts) {solution in [python](/algorithms/warmup/time_conversion.py)}

### Implementation
* [Easy] [Grading Students](https://www.hackerrank.com/challenges/grading) (10 pts) {solution in [python](/algorithms/implementation/grading_students.py)}
* [Easy] [Apple and Orange](https://www.hackerrank.com/challenges/apple-and-orange) (10 pts) {solution in [python](/algorithms/implementation/apple_and_orange.py)}
* [Easy] [Kangaroo](https://www.hackerrank.com/challenges/kangaroo) (10 pts) {solution in [python](/algorithms/implementation/kangaroo.py)}
* [Easy] [Between Two Sets](https://www.hackerrank.com/challenges/between-two-sets) (10 pts) {solution in [python](/algorithms/implementation/between_two_sets.py)}
* [Easy] [Breaking the Records](https://www.hackerrank.com/challenges/breaking-best-and-worst-records) (10 pts) {solution in [python](/algorithms/implementation/breaking_the_records.py)}
* [Easy] [Birthday Chocolate](https://www.hackerrank.com/challenges/the-birthday-bar) (10 pts) {solution in [python](/algorithms/implementation/birthday_chocolate.py)}
* [Easy] [Divisible Sum Pairs](https://www.hackerrank.com/challenges/divisible-sum-pairs) (10 pts) {solution in [python](/algorithms/implementation/divisible_sum_pairs.py)}
* [Easy] [Migratory Birds](https://www.hackerrank.com/challenges/migratory-birds) (10 pts) {solution in [python](/algorithms/implementation/migratory_birds.py)}
* [Easy] [Day of the Programmer](https://www.hackerrank.com/challenges/day-of-the-programmer) (15 pts) {solution in [python](/algorithms/implementation/day_of_the_programmer.py)}
* [Easy] [Bon Appetit](https://www.hackerrank.com/challenges/bon-appetit) (10 pts) {solution in [python](/algorithms/implementation/bon_appetit.py)}
* [Easy] [Sock Merchant](https://www.hackerrank.com/challenges/sock-merchant) (10 pts) {solution in [python](/algorithms/implementation/sock_merchant.py)}
* [Easy] [Drawing Book](https://www.hackerrank.com/challenges/drawing-book) (10 pts) {solution in [python](/algorithms/implementation/drawing_book.py)}
* [Easy] [Counting Valleys](https://www.hackerrank.com/challenges/counting-valleys) (15 pts) {solution in [python](/algorithms/implementation/counting_valleys.py)}
* [Easy] [Electronics Shop](https://www.hackerrank.com/challenges/electronics-shop) (15 pts) {solution in [python](/algorithms/implementation/electronics_shop.py)}
* [Easy] [Cats and a Mouse](https://www.hackerrank.com/challenges/cats-and-a-mouse) (15 pts) {solution in [python](/algorithms/implementation/cats_and_a_mouse.py)}
* [Easy] [Picking Numbers](https://www.hackerrank.com/challenges/picking-numbers) (20 pts) {solution in [python](/algorithms/implementation/picking_numbers.py)}
* [Medium] [Climbing the Leaderboard](https://www.hackerrank.com/challenges/climbing-the-leaderboard) (20 pts) {solution in [python](/algorithms/implementation/climbing_the_leaderboard.py)}
* [Easy] [The Hurdle Race](https://www.hackerrank.com/challenges/the-hurdle-race) (15 pts) {solution in [python](/algorithms/implementation/the_hurdle_race.py)}
* [Easy] [Designer PDF Viewer](https://www.hackerrank.com/challenges/designer-pdf-viewer) (20 pts) {solution in [python](/algorithms/implementation/designer_pdf_viewer.py)}
* [Easy] [Utopian Tree](https://www.hackerrank.com/challenges/utopian-tree) (20 pts) {solution in [python](/algorithms/implementation/utopian_tree.py)}
* [Easy] [Angry Professor](https://www.hackerrank.com/challenges/angry-professor) (20 pts) {solution in [python](/algorithms/implementation/angry_professor.py)}
* [Easy] [Beautiful Days at the Movies](https://www.hackerrank.com/challenges/beautiful-days-at-the-movies) (15 pts) {solution in [python](/algorithms/implementation/beautiful_days_at_the_movies.py)}
* [Easy] [Viral Advertising](https://www.hackerrank.com/challenges/strange-advertising) (15 pts) {solution in [python](/algorithms/implementation/viral_advertising.py)}
* [Easy] [Save the Prisoner!](https://www.hackerrank.com/challenges/save-the-prisoner) (15 pts) {solution in [python](/algorithms/implementation/save_the_prisoner.py)}
* [Easy] [Circular Array Rotation](https://www.hackerrank.com/challenges/circular-array-rotation) (20 pts) {solution in [python](/algorithms/implementation/circular_array_rotation.py)}
* [Easy] [Sequence Equation](https://www.hackerrank.com/challenges/permutation-equation) (20 pts) {solution in [python](/algorithms/implementation/sequence_equation.py)}
* [Easy] [Jumping on the Clouds: Revisited](https://www.hackerrank.com/challenges/jumping-on-the-clouds-revisited) (15 pts) {solution in [python](/algorithms/implementation/jumping_on_the_clouds_revisited.py)}
* [Easy] [Find Digits](https://www.hackerrank.com/challenges/find-digits) (25 pts) {solution in [python](/algorithms/implementation/find_digits.py)}
* [Medium] [Extra Long Factorials](https://www.hackerrank.com/challenges/extra-long-factorials) (20 pts) {solution in [python](/algorithms/implementation/extra_long_factorials.py)}
* [Easy] [Append and Delete](https://www.hackerrank.com/challenges/append-and-delete) (20 pts) {solution in [python](/algorithms/implementation/append_and_delete.py)}
* [Easy] [Sherlock and Squares](https://www.hackerrank.com/challenges/sherlock-and-squares) (20 pts) {solution in [python](/algorithms/implementation/sherlock_and_squares.py)}
* [Easy] [Library Fine](https://www.hackerrank.com/challenges/library-fine) (15 pts) {solution in [python](/algorithms/implementation/library_fine.py)}
* [Easy] [Cut the Sticks](https://www.hackerrank.com/challenges/cut-the-sticks) (25 pts) {solution in [python](/algorithms/implementation/cut_the_sticks.py)}
* [Easy] [Repeated String](https://www.hackerrank.com/challenges/repeated-string) (20 pts) {solution in [python](/algorithms/implementation/repeated_string.py)}
* [Easy] [Jumping on the Clouds](https://www.hackerrank.com/challenges/jumping-on-the-clouds) (20 pts) {solution in [python](/algorithms/implementation/jumping_on_the_clouds.py)}
* [Easy] [Equalize the Array](https://www.hackerrank.com/challenges/equality-in-a-array) (20 pts) {solution in [python](/algorithms/implementation/equalize_the_array.py)}

### Strings
* [Easy] [Super Reduced String](https://www.hackerrank.com/challenges/reduced-string) (10 pts) {solution in [python](/algorithms/strings/super_reduced_string.py)}
* [Easy] [CamelCase](https://www.hackerrank.com/challenges/camelcase) (15 pts) {solution in [python](/algorithms/strings/camel_case.py)}
* [Easy] [Strong Password](https://www.hackerrank.com/challenges/strong-password) (15 pts) {solution in [python](/algorithms/strings/strong_password.py)}
* [Easy] [Two Characters](https://www.hackerrank.com/challenges/two-characters) (20 pts) {solution in [python](/algorithms/strings/two_characters.py)}
* [Easy] [Caesar Cipher](https://www.hackerrank.com/challenges/caesar-cipher-1) (15 pts) {solution in [python](/algorithms/strings/caesar_cipher.py)}
* [Easy] [Mars Exploration](https://www.hackerrank.com/challenges/mars-exploration) (15 pts) {solution in [python](/algorithms/strings/mars_exploration.py)}
* [Easy] [HackerRank in a String!](https://www.hackerrank.com/challenges/hackerrank-in-a-string) (20 pts) {solution in [python](/algorithms/strings/hacker_rank_in_a_string.py)}
* [Easy] [Pangrams](https://www.hackerrank.com/challenges/pangrams) (20 pts) {solution in [python](/algorithms/strings/pangrams.py)}
* [Easy] [Weighted Uniform Strings](https://www.hackerrank.com/challenges/weighted-uniform-string) (20 pts) {solution in [python](/algorithms/strings/weighted_uniform_strings.py)}
* [Easy] [Funny String](https://www.hackerrank.com/challenges/funny-string) (25 pts) {solution in [python](/algorithms/strings/funny_string.py)}
* [Easy] [Gemstones](https://www.hackerrank.com/challenges/gem-stones) (20 pts) {solution in [python](/algorithms/strings/gemstones.py)}
* [Easy] [Alternating Characters](https://www.hackerrank.com/challenges/alternating-characters) (20 pts) {solution in [python](/algorithms/strings/alternating_characters.py)}
* [Easy] [Beautiful Binary String](https://www.hackerrank.com/challenges/beautiful-binary-string) (20 pts) {solution in [python](/algorithms/strings/beautiful_binary_string.py)}