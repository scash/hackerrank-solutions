#!/bin/python3

def miniMaxSum(arr):
    maximum = sum(arr) - min(arr)
    minimum = sum(arr) - max(arr)

    print(minimum, maximum)



if __name__ == '__main__':
    arr = list(map(int, input().rstrip().split()))

    miniMaxSum(arr)
