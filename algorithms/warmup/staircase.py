#!/bin/python3

def staircase(n):
    counter = 1

    while n >= counter:
        row = "#" * counter
        print(row.rjust(n, " "))
        counter += 1

    #
    # Alternate solution
    #
    # for r in range(n):
    #     for c in range(n):
    #         if c+1 < n-r:
    #             print(" ", end="")
    #         else:
    #             print("#", end="")

    #     print()



if __name__ == '__main__':
    n = int(input())

    staircase(n)
