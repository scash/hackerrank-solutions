#!/bin/python3

def plusMinus(arr):
    divisor = len(arr)
    pos, neg, zer = 0, 0, 0

    for i in range(divisor):
        if arr[i] > 0:
            pos += 1
        elif arr[i] < 0:
            neg += 1
        else:
            zer += 1

    print("{:.6f}".format(pos / divisor))
    print("{:.6f}".format(neg / divisor))
    print("{:.6f}".format(zer / divisor))



if __name__ == '__main__':
    n = int(input())

    arr = list(map(int, input().rstrip().split()))

    plusMinus(arr)
