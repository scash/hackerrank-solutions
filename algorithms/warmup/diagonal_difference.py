#!/bin/python3

import os

#
# The function is expected to return an INTEGER.
# The function accepts 2D_INTEGER_ARRAY arr as parameter.
#
def diagonalDifference(arr):
    left, right = list(), list()

    for i in range(len(arr)):
        left.append(arr[i][i])
        right.append(arr[i][(len(arr) - 1) - i])

    return abs(sum(left) - sum(right))



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    arr = []

    for _ in range(n):
        arr.append(list(map(int, input().rstrip().split())))

    result = diagonalDifference(arr)

    fptr.write(str(result) + '\n')

    fptr.close()
