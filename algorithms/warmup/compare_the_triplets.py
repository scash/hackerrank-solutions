#!/bin/python3

import os

def compareTriplets(a, b):
    a_tally, b_tally = 0, 0

    for i, j in zip(a, b):

        if i > j:
            a_tally += 1
        elif j > i:
            b_tally += 1

    return [a_tally, b_tally]



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    a = list(map(int, input().rstrip().split()))

    b = list(map(int, input().rstrip().split()))

    result = compareTriplets(a, b)

    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
