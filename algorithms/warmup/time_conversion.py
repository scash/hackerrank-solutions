#!/bin/python3

import os

def timeConversion(s):
    output = ""
    am_pm = s[-2:]
    hour = int(s[:2])

    if am_pm.upper() == "PM":

        # Handle noon
        if hour == 12:
            output = s[:-2]
        else:
            mil_hour = str(hour + 12)

            output = mil_hour + s[2:-2]
    else:

        # Handle midnight
        if hour == 12:
            output = "00" + s[2:-2]
        else:
            output = s[:-2]

    return(output)



if __name__ == '__main__':
    f = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = timeConversion(s)

    f.write(result + '\n')

    f.close()
