#!/bin/python3

import os

def marsExploration(s):
    output = 0
    messages = len(s) // 3

    for i in range(messages):
        msg = s[i*3:(i*3)+3]

        if msg[0] != "S":
            output += 1

        if msg[1] != "O":
            output += 1

        if msg[2] != "S":
            output += 1

    return output



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = marsExploration(s)

    fptr.write(str(result) + '\n')

    fptr.close()
