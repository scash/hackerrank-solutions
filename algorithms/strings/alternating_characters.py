#!/bin/python3

import os

def alternatingCharacters(s):
    s_a, s_b = "AA", "BB"
    index, counter = -1, -2

    while True:
        counter += 1
        index = s.find(s_a, index + 1)

        if index == -1:
            break

    index = -1

    while True:
        counter += 1
        index = s.find(s_b, index + 1)

        if index == -1:
            break

    return counter



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input())

    for q_itr in range(q):
        s = input()

        result = alternatingCharacters(s)

        fptr.write(str(result) + '\n')

    fptr.close()
