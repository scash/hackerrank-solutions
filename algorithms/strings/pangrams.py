#!/bin/python3

import os

def pangrams(s):
    alphabet = {chr(i) for i in range(ord("a"), ord("z") + 1)}

    s_set = set(s.lower())

    # Determine if there are any letters in the
    #   alphabet not contained in the string
    if len(alphabet - s_set) == 0:
        return "pangram"
    else:
        return "not pangram"



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = pangrams(s)

    fptr.write(result + '\n')

    fptr.close()
