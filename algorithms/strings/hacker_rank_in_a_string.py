#!/bin/python3

import os

def hackerrankInString(s):
    index = -1
    counter = 0

    hr = "hackerrank"
    hr_set = set(hr)

    # Ensure string contains all necessary characters
    if len(hr_set - set(s)) == 0:

        # For each character we need to locate,
        #   attempt to find in sequential order
        for c in hr:
            index = s.find(c, index + 1)

            if index != -1:
                counter += 1
            else:
                break

        if counter == len(hr):
            return "YES"

    return "NO"



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input())

    for q_itr in range(q):
        s = input()

        result = hackerrankInString(s)

        fptr.write(result + '\n')

    fptr.close()
