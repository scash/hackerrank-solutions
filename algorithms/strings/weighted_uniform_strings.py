#!/bin/python3

import os
from collections import defaultdict

def weighted_uniform_strings(s, queries):
    prev = ""
    output = list()
    weights = dict()
    weight_values = set()
    length_counter, weight_counter = 0, 1
    char_longest_uniform_string = defaultdict(int)

    # Set character weight values
    for val in range(ord("a"), ord("z") + 1):
        weights[chr(val)] = weight_counter

        weight_counter += 1

    # Tally length of each uniform string
    for c in s:
        if c != prev:
            prev = c
            length_counter = 1
        else:
            length_counter += 1

        if length_counter > char_longest_uniform_string[c]:
            char_longest_uniform_string[c] = length_counter

    # Traverse set of charaters contained within supplied string
    for k, v in char_longest_uniform_string.items():

        # Add weight values for all uniform strings
        #   composed of the specified character
        for i in range(1, v + 1):
            weight_values.add(weights[k] * i)

    # Determine if query value within set of weight values
    for query in queries:
        if query in weight_values:
            output.append("Yes")
        else:
            output.append("No")

    return output



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    queries_count = int(input())

    queries = []

    for _ in range(queries_count):
        queries_item = int(input())
        queries.append(queries_item)

    result = weighted_uniform_strings(s, queries)

    fptr.write('\n'.join(result))
    fptr.write('\n')

    fptr.close()
