#!/bin/python3

import os

def minimumNumber(n, password):
    numbers = set("0123456789")
    special_characters = set("!@#$%^&*()-+")
    lower_case = set("abcdefghijklmnopqrstuvwxyz")
    upper_case = set("ABCDEFGHIJKLMNOPQRSTUVWXYZ")

    need = 0
    pass_set = set(password)

    # Is the password missing a numeric value?
    if len(numbers - pass_set) == len(numbers):
        need += 1

    # Is the password missing a special character value?
    if len(special_characters - pass_set) == len(special_characters):
        need += 1

    # Is the password missing a lower case value?
    if len(lower_case - pass_set) == len(lower_case):
        need += 1

    # Is the password missing an upper case value?
    if len(upper_case - pass_set) == len(upper_case):
        need += 1

    if len(password) >= 6 or (len(password) + need) >= 6:
        return need
    else:
        short_by = 6 - len(password)

        if short_by >= need:
            return short_by
        else:
            return need



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    password = input()

    answer = minimumNumber(n, password)

    fptr.write(str(answer) + '\n')

    fptr.close()
