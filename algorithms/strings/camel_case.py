#!/bin/python3

import os

def camelcase(s):
    output = 1

    for i in range(len(s)):
        if s[i].isupper():
            output += 1

    return output



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = camelcase(s)

    fptr.write(str(result) + '\n')

    fptr.close()
