#!/bin/python3

import os

def funnyString(s):
    rev_s = s[::-1]

    for i in range(len(s) - 1):
        if abs(ord(s[i]) - ord(s[i + 1])) != abs(ord(rev_s[i]) - ord(rev_s[i + 1])):
            return "Not Funny"

    return "Funny"



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input())

    for q_itr in range(q):
        s = input()

        result = funnyString(s)

        fptr.write(result + '\n')

    fptr.close()
