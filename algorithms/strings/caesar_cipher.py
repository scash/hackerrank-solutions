#!/bin/python3

import os

def caesarCipher(s, k):
    output = ""
    rotated_alphabet = list()
    alphabet = [chr(i) for i in range(ord("a"), ord("z") + 1)]

    rotations = k % 26

    if rotations > 0:
        rotated_alphabet = alphabet[rotations:] + alphabet[:rotations]
    else:
        # When number of rotations is a multiple of 26,
        #   then there is no need to rotate the alphabet
        rotated_alphabet = alphabet[:]

    for c in s:
        c_lower = c.lower()

        if c_lower in alphabet:
            new_c = rotated_alphabet[alphabet.index(c_lower)]

            if c.isupper():
                output += new_c.upper()
            else:
                output += new_c
        else:
            output += c

    return output



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    s = input()

    k = int(input())

    result = caesarCipher(s, k)

    fptr.write(result + '\n')

    fptr.close()
