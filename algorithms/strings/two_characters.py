#!/bin/python3

import os

def alternate(s):
    output = 0
    pairs = list()
    unique_chars = list(set(s))

    # Loop through all possible character pairs
    for i in range(len(unique_chars)):
        for j in range(i + 1, len(unique_chars)):

            # Obtain list from s with only the set of specified characters
            only_two = [x for x in s if x in [unique_chars[i], unique_chars[j]]]

            #############################################
            # Determine pairs that are worth checking.
            #   Note that this doesn't guarantee the entry is legitimate, but
            #   it lessens the number of pairs we need to validate as eligible.
            #############################################

            if len(only_two) % 2 == 0:

                # When the length is even, then the count of any one of the two characters
                #   should be exactly half the length
                if only_two.count(unique_chars[i]) == (len(only_two) / 2):
                    pairs.append(only_two)
            else:

                # When the length is odd, then the count difference amongst the two characters
                #   should be exactly one
                if abs(only_two.count(unique_chars[i]) - only_two.count(unique_chars[j])) == 1:
                    pairs.append( only_two)

    # Perform a more thorough check on pairs that passed initial screen
    for pair in pairs:
        length = len(pair)

        for i in range(1, length):
            if pair[i-1] == pair[i]:
                length = 0
                break

        if length > output:
            output = length

    return output



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    l = int(input().strip())

    s = input()

    result = alternate(s)

    fptr.write(str(result) + '\n')

    fptr.close()
