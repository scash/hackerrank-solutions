#!/bin/python3

import os

def gemstones(arr):
    gems = 0

    for i in range(ord("a"), ord("z") + 1):
        counter = 0

        for j in range(len(arr)):
            if chr(i) not in arr[j]:
                break

            counter += 1

        if counter == len(arr):
            gems += 1

    return gems



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    arr = []

    for _ in range(n):
        arr_item = input()
        arr.append(arr_item)

    result = gemstones(arr)

    fptr.write(str(result) + '\n')

    fptr.close()
