#!/bin/python3

import os

def beautifulBinaryString(b):
    index, counter = 0, 0

    while True:
        index = b.find("010", index)

        if index != -1:
            index += 3
            counter += 1
        else:
            break

    return counter



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    b = input()

    result = beautifulBinaryString(b)

    fptr.write(str(result) + '\n')

    fptr.close()
