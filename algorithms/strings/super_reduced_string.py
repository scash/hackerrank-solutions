#!/bin/python3

import os

def super_reduced_string(s):
    position = 0
    sl = list(s)

    while len(sl) > position:
        if position + 1 < len(sl):
            if sl[position] == sl[position + 1]:
                del sl[position:position + 2]

                # When delete operation performed, if applicable,
                #   backtrack one position, to check previous
                #   character with new adjacent character
                if position != 0:
                    position -= 1

                continue

        position += 1

    if len(sl) == 0:
        return "Empty String"
    else:
        return "".join(sl)



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = super_reduced_string(s)

    fptr.write(result + '\n')

    fptr.close()
