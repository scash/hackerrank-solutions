#!/bin/python3

import os

def beautifulDays(i, j, k):
    days = 0

    for day in range(i, j+1):

        # Obtain reverse
        #   NOTE: Will not work with negative values, however,
        #   problem constraints state that will never be the case
        reversed_day = int(str(day)[::-1])

        if abs(day - reversed_day) % k == 0:
            days += 1

    return days



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    ijk = input().split()

    i = int(ijk[0])

    j = int(ijk[1])

    k = int(ijk[2])

    result = beautifulDays(i, j, k)

    fptr.write(str(result) + '\n')

    fptr.close()
