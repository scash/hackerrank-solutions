#!/bin/python3

import os

def designerPdfViewer(h, word):
    counter = 0
    alphabet = dict()
    word_values = list()

    # Traverse alphabet, store letter values in a dict
    for i in range(ord("a"), ord("z") + 1):
        alphabet[chr(i)] = h[counter]
        counter += 1

    # Obtain numeric value of each character within the word
    for c in word.lower():
        word_values.append(alphabet[c])

    return max(word_values) * len(word)



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    h = list(map(int, input().rstrip().split()))

    word = input()

    result = designerPdfViewer(h, word)

    fptr.write(str(result) + '\n')

    fptr.close()
