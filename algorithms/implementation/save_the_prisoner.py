#!/bin/python3

import os

def saveThePrisoner(n, m, s):
    remain = 0

    # Determine remaining treats on last round
    if m > n:
        remain = m % n
    else:
        remain = m

    position = (remain + s) - 1

    # If we've overshot position, then adjust
    if position > n:
        position -= n
    elif position == 0:
        position = n

    return position



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    t = int(input())

    for t_itr in range(t):
        nms = input().split()

        n = int(nms[0])

        m = int(nms[1])

        s = int(nms[2])

        result = saveThePrisoner(n, m, s)

        fptr.write(str(result) + '\n')

    fptr.close()
