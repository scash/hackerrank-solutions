#!/bin/python3

def bonAppetit(bill, k, b):
    should_pay = (sum(bill) - bill[k]) / 2

    if should_pay != b:
        return int(b - should_pay)
    else:
        return "Bon Appetit"



if __name__ == '__main__':
    nk = input().rstrip().split()

    n = int(nk[0])

    k = int(nk[1])

    bill = list(map(int, input().rstrip().split()))

    b = int(input().strip())

    print(bonAppetit(bill, k, b))
