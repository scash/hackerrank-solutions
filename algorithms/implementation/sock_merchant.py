#!/bin/python3

import os
from collections import defaultdict

def sockMerchant(n, ar):
    counter = 0
    sock_tally = defaultdict(int)

    # Tally socks
    for item in ar:
        sock_tally[item] += 1

        # If an even number of socks, then we got a pair
        if sock_tally[item] % 2 == 0:
            counter += 1

    return counter



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    ar = list(map(int, input().rstrip().split()))

    result = sockMerchant(n, ar)

    fptr.write(str(result) + '\n')

    fptr.close()
