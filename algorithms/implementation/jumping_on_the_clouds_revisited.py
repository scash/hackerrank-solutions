#!/bin/python3

import os

def jumpingOnClouds(c, k):
    size = len(c)
    energy, index = 100, 0

    while True:

        # Deduct jump
        energy -= 1

        # Determine next index
        index = (index + k) % size

        # Deduct landing on thunder cloud
        if c[index]:
            energy -= 2

        # Terminate when landing back at start
        if index == 0:
            break

    return energy



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    c = list(map(int, input().rstrip().split()))

    result = jumpingOnClouds(c, k)

    fptr.write(str(result) + '\n')

    fptr.close()
