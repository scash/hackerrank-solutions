#!/bin/python3

import os

def dayOfProgrammer(year):
    leap = "12.09."
    common = "13.09."
    year_1918 = "26.09."

    if year < 1918:
        if year % 4 == 0:
            return leap + str(year)
        else:
            return common + str(year)
    elif year > 1918:
        if (year % 400 == 0) or ((year % 4 == 0) and not (year % 100 == 0)):
            return leap + str(year)
        else:
            return common + str(year)
    else:
        return year_1918 + str(year)



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    year = int(input().strip())

    result = dayOfProgrammer(year)

    fptr.write(result + '\n')

    fptr.close()
