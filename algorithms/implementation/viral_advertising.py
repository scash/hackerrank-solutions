#!/bin/python3

import os

def viralAdvertising(n):
    recipients, likes, output = 5, 0, 0

    for _ in range(n):
        likes = recipients // 2
        recipients = likes * 3

        output += likes

    return output



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    result = viralAdvertising(n)

    fptr.write(str(result) + '\n')

    fptr.close()
