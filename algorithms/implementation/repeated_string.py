#!/bin/python3

import os

def repeatedString(s, n):
    partial = 0

    mod_val = n % len(s)
    quotient = n // len(s)
    a_count = s.count("a")

    if mod_val != 0:
        partial = s[:mod_val].count("a")

    return (a_count * quotient) + partial



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    n = int(input())

    result = repeatedString(s, n)

    fptr.write(str(result) + '\n')

    fptr.close()
