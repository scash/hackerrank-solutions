#!/bin/python3

import os

def permutationEquation(p):
    data = dict()
    output = list()

    # Create a dict in which keys are array elements
    # and values are the position of those elements
    for i in range(len(p)):
        data[p[i]] = i + 1

    for j in range(1, len(p) + 1):
        output.append(data[data[j]])

    return output



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    p = list(map(int, input().rstrip().split()))

    result = permutationEquation(p)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
