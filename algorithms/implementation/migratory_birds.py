#!/bin/python3

import os
from collections import defaultdict

def migratoryBirds(arr):
    types = defaultdict(int)

    # Tally bird type sightings
    for i in range(len(arr)):
        types[arr[i]] += 1

    highest_freq = max(types.values())

    # Obtain ID of bird types that match highest frequency sighting
    freq_types = [k for k in types if types[k] == highest_freq]

    return min(freq_types)



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    arr_count = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    result = migratoryBirds(arr)

    fptr.write(str(result) + '\n')

    fptr.close()
