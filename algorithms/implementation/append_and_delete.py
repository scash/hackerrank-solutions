#!/bin/python3

import os

def appendAndDelete(s, t, k):
    counter = 0
    len_s = len(s)
    len_t = len(t)

    # When the number of operations is >= the combined
    #   length of both strings, then conversion is possible.
    #   No need to proceed further.
    if k >= (len_s + len_t):
        return "Yes"

    # Determine string with shortest length
    shortest = min([len_s, len_t])

    # Left-to-right, compare strings to determine
    # number of characters that are identical, if any
    for i in range(shortest):
        if s[i] == t[i]:
            counter += 1
        else:
            break

    difference = (len_s - counter) + (len_t - counter)

    if k == difference:
        return "Yes"
    elif k > difference:

        # When k is greater than the difference, then both numbers
        #   must be either even or odd to be able to convert
        if k % 2 == 0:
            if difference % 2 == 0:
                 return "Yes"
            else:
                return "No"
        else:
            if difference % 2 == 0:
                 return "No"
            else:
                return "Yes"
    else:
        return "No"



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    t = input()

    k = int(input())

    result = appendAndDelete(s, t, k)

    fptr.write(result + '\n')

    fptr.close()
