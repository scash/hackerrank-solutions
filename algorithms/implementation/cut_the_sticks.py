#!/bin/python3

import os

def cutTheSticks(arr):
    output = list()

    while len(arr) > 0:
        retain = list()
        output.append(len(arr))

        short_stick = min(arr)

        # Subtract length of short stick from remaining sticks
        for i in range(len(arr)):
            arr[i] -= short_stick

            if arr[i] > 0:
                retain.append(arr[i])

        arr = retain

    return output



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    arr = list(map(int, input().rstrip().split()))

    result = cutTheSticks(arr)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
