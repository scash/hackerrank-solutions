#!/bin/python3

import os
import math

def pageCount(n, p):
    from_end = 0
    from_beginning = p // 2

    if n % 2 == 0:
        if p % 2 == 0:
            from_end = int((n - p) / 2)
        else:
            from_end = math.ceil((n - p) / 2)
    else:
        if p % 2 == 0:
            from_end = (n - p) // 2
        else:
            from_end = int((n - p) / 2)

    return min([from_beginning, from_end])



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    p = int(input())

    result = pageCount(n, p)

    fptr.write(str(result) + '\n')

    fptr.close()
