#!/bin/python3

import os

def getMoneySpent(keyboards, drives, b):
    combinations = list()

    # Budget not sufficient even for lowest combination
    if (min(drives) + min(keyboards)) > b:
        return -1

    # Account for all possible combinations
    for i in range(len(drives)):
        for j in range(len(keyboards)):
            combinations.append(drives[i] + keyboards[j])

    # Sort combinations
    combinations.sort(reverse=True)

    for cost in combinations:
        if cost <= b:
            return cost



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    bnm = input().split()

    b = int(bnm[0])

    n = int(bnm[1])

    m = int(bnm[2])

    keyboards = list(map(int, input().rstrip().split()))

    drives = list(map(int, input().rstrip().split()))

    #
    # The maximum amount of money she can spend on a keyboard and USB drive, or -1 if she can't purchase both items
    #

    moneySpent = getMoneySpent(keyboards, drives, b)

    fptr.write(str(moneySpent) + '\n')

    fptr.close()
