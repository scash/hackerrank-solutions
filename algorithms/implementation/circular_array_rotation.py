#!/bin/python3

import os

def circularArrayRotation(a, k, queries):
    output, rotated = list(), list()

    # When requested rotations is >= the size of the array,
    #   we find the remainder to limit the number of performed
    #   rotations. For example, if k = 17 and array is of size 5,
    #   then k should be assigned 2 (hence rotating twice) instead
    #   of 17 times...
    if k >= len(a):
        k %= len(a)

    # Split and shift array items (to "mimic" rotation)
    if k != 0:
        pos = len(a) - k
        rotated = a[pos:] + a[:pos]
    else:
        # Performing the requested rotations results in the same array
        rotated = a[:]

    for query in queries:
        output.append(rotated[query])

    return output



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nkq = input().split()

    n = int(nkq[0])

    k = int(nkq[1])

    q = int(nkq[2])

    a = list(map(int, input().rstrip().split()))

    queries = []

    for _ in range(q):
        queries_item = int(input())
        queries.append(queries_item)

    result = circularArrayRotation(a, k, queries)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
