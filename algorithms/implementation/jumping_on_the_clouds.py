#!/bin/python3

import os

def jumpingOnClouds(c):
    jumps, position = 0, 0

    while position < len(c) - 1:

        # Ensure max jump position is within bounds and of value zero 
        if (position + 2) < len(c) and c[position + 2] == 0:
            position += 2
        else:
            position += 1

        jumps += 1

    return jumps



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    c = list(map(int, input().rstrip().split()))

    result = jumpingOnClouds(c)

    fptr.write(str(result) + '\n')

    fptr.close()
