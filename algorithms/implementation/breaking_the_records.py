#!/bin/python3

import os

def breakingRecords(scores):
    count_minimum, minimum_val, count_maximum, maximum_val = 0, 0, 0, 0

    for i in range(len(scores)):
        if i:
            if scores[i] > maximum_val:
                count_maximum += 1
                maximum_val = scores[i]

            if scores[i] < minimum_val:
                count_minimum += 1
                minimum_val = scores[i]
        else:
            minimum_val = scores[i]
            maximum_val = scores[i]

    return [count_maximum, count_minimum]



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    scores = list(map(int, input().rstrip().split()))

    result = breakingRecords(scores)

    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
