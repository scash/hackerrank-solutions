#!/bin/python3

def countApplesAndOranges(s, t, a, b, apples, oranges):
    apple_tally, orange_tally = 0, 0

    for i in range(len(apples)):
        landing_val = a + apples[i]

        if landing_val >= s and landing_val <= t:
            apple_tally += 1

    for j in range(len(oranges)):
        landing_val = b + oranges[j]

        if landing_val >= s and landing_val <= t:
            orange_tally += 1

    print(apple_tally)
    print(orange_tally)



if __name__ == '__main__':
    st = input().split()

    s = int(st[0])

    t = int(st[1])

    ab = input().split()

    a = int(ab[0])

    b = int(ab[1])

    mn = input().split()

    m = int(mn[0])

    n = int(mn[1])

    apples = list(map(int, input().rstrip().split()))

    oranges = list(map(int, input().rstrip().split()))

    countApplesAndOranges(s, t, a, b, apples, oranges)
