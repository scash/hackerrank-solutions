#!/bin/python3

import os

def findDigits(n):
    counter = 0

    # Create a list of digits based on n
    digits = list(map(int, str(n)))

    unique = set(digits)

    for digit in unique:
        if digit != 0 and (n % digit) == 0:
            counter += digits.count(digit)

    return counter



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    t = int(input())

    for t_itr in range(t):
        n = int(input())

        result = findDigits(n)

        fptr.write(str(result) + '\n')

    fptr.close()
