#!/bin/python3

import os

#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY a as parameter.
#
def pickingNumbers(a):
    counts = list()

    for num in a:

        # For the given integer, determine count of
        #   the absolute difference ceiling
        counts.append(a.count(num) + a.count(num + 1))

        # For the given integer, determine count of
        #   the absolute difference floor
        counts.append(a.count(num) + a.count(num - 1))

    return max(counts)



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    a = list(map(int, input().rstrip().split()))

    result = pickingNumbers(a)

    fptr.write(str(result) + '\n')

    fptr.close()
