#!/bin/python3

import os

def utopianTree(n):
    output = 1

    for i in range(n):
        if i % 2 == 0:
            output *= 2
        else:
            output += 1

    return output



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    t = int(input())

    for t_itr in range(t):
        n = int(input())

        result = utopianTree(n)

        fptr.write(str(result) + '\n')

    fptr.close()
