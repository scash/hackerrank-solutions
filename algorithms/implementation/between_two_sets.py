#!/bin/python3

import os

#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER_ARRAY a
#  2. INTEGER_ARRAY b
#
def getTotalX(a, b):
    all_factors = list()
    common_factors = set()

    # Obtain all factors from items in B
    for item in b:
        factors_per_item = set()

        for i in range(1, item + 1):
            if item % i == 0:
                factors_per_item.add(i)

        all_factors.append(factors_per_item)

    # Determine common factors across items in B
    for i in range(len(all_factors)):

        if common_factors:
            common_factors &= all_factors[i]
        else:
            common_factors = all_factors[i]

    # Create dictionary from unique set of factors
    unique_factors = dict.fromkeys(common_factors, 0)

    # For all common factors in B, determine which are factors of items in A
    for f in unique_factors:
        for item in a:

            # Is item from A a factor of a common factor from B?
            if f % item == 0:
                unique_factors[f] += 1

    # All eligible common factors that equal the number of items in A
    #   are said to be between the two arrays
    return list(unique_factors.values()).count(len(a))



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    m = int(first_multiple_input[1])

    arr = list(map(int, input().rstrip().split()))

    brr = list(map(int, input().rstrip().split()))

    total = getTotalX(arr, brr)

    fptr.write(str(total) + '\n')

    fptr.close()
