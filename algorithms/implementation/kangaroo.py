#!/bin/python3

import os

def kangaroo(x1, v1, x2, v2):
    jump = 1

    # Handle case when a kangaroo is ahead of the other
    # and moves a higher distance per jump
    if x2 > x1 and v2 > v1:
        return("NO")

    # Handle case when a kangaroo is ahead of the other
    # but move at the same velocity
    if x2 > x1 and v2 == v1:
        return("NO")

    while True:
        x1_location = x1 + (v1 * jump)
        x2_location = x2 + (v2 * jump)

        if x1_location == x2_location:
            return("YES")

        # If the x1 location becomes greater than x2 location,
        #   the 2nd kangaroo will not catch-up because
        #   v2 is lower than v1
        if x1_location > x2_location:
            return("NO")

        jump += 1

    return("NO")



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    x1V1X2V2 = input().split()

    x1 = int(x1V1X2V2[0])

    v1 = int(x1V1X2V2[1])

    x2 = int(x1V1X2V2[2])

    v2 = int(x1V1X2V2[3])

    result = kangaroo(x1, v1, x2, v2)

    fptr.write(result + '\n')

    fptr.close()
