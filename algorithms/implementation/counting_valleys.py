#!/bin/python3

import os

def countingValleys(n, s):
    in_valley = False
    position, valleys = 0, 0

    # Convert steps into a numeric list
    arr = [1 if c == "U" else -1 for c in s]

    for step in arr:
        position += step

        if not in_valley and position < 0:
            valleys += 1
            in_valley = True
        elif position >= 0:
            in_valley = False
        else:
            # A step down on an accounted valley, do nothing
            pass

    return valleys



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    s = input()

    result = countingValleys(n, s)

    fptr.write(str(result) + '\n')

    fptr.close()
