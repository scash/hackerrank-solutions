#!/bin/python3

import os
import math

def squares(a, b):
    low, high = 0, 0

    # Determine lowest square integer within the range
    for i in range(a, b + 1):
        if math.sqrt(i).is_integer():
            low = int(math.sqrt(i))
            break

    # Determine highest square integer within the range
    for i in range(b, a - 1, -1):
        if math.sqrt(i).is_integer():
            high = int(math.sqrt(i))
            break

    return 0 if low + high == 0 else (high - low) + 1



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input())

    for q_itr in range(q):
        ab = input().split()

        a = int(ab[0])

        b = int(ab[1])

        result = squares(a, b)

        fptr.write(str(result) + '\n')

    fptr.close()
