#!/bin/python3

import os

def climbingLeaderboard(scores, alice):
    index = 0
    output = list()

    # Reverse Alice's scores, so they're in descending order
    alice = list(reversed(alice))

    # Eliminate duplicates from the leaderboard
    scores = list(set(scores))

    # Apply descending order sort after list->set->list conversion
    scores.sort(reverse=True)

    for s in alice:
        rank = index

        for i in range(index, len(scores)):
            if s < scores[i]:
                rank += 1

                # Handle case when Alice's score is lower
                #   than lowest leaderboard score
                if i == (len(scores) - 1):
                    rank += 1

                index = rank
            else:
                rank += 1
                break

        output.append(rank)

    # Output should be reverted back to ascending order
    return list(reversed(output))



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    scores_count = int(input())

    scores = list(map(int, input().rstrip().split()))

    alice_count = int(input())

    alice = list(map(int, input().rstrip().split()))

    result = climbingLeaderboard(scores, alice)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
