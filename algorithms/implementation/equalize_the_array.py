#!/bin/python3

import os

def equalizeArray(arr):
    counts = dict()
    unique_items = set(arr)

    # For each unique element, determine count
    for item in unique_items:
        counts[item] = arr.count(item)

    most_frequent = max(counts.values())

    return len(arr) - most_frequent



if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    arr = list(map(int, input().rstrip().split()))

    result = equalizeArray(arr)

    fptr.write(str(result) + '\n')

    fptr.close()
